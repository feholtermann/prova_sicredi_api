package com.felipeholtermann.ProvaSicrediApi;

import static io.restassured.RestAssured.given;

import java.util.Scanner;

import org.junit.Test;

import io.restassured.response.Response;

public class TesteCep {
	public String cep;
	public Scanner entrada;

	@Test
	public void validacaoCEP() {
		System.out.println("Teste de busca pelo cep");
		entrada = new Scanner(System.in);
		System.out.println("Informe o CEP");
		cep = entrada.next();

		Response response = given().relaxedHTTPSValidation().when().get("https://viacep.com.br/ws/" + cep + "/json/");
		response.getStatusCode();

		if (response.body().asString().contains("erro")) {
			System.out.println("O CEP informado é inválido");
		} else if (response.getStatusCode() == 400) {
			System.out.println("O CEP informado está fora do formato aceito.");
		} else if (!response.body().asString().contains("erro") && (response.getStatusCode() == 200)) {
			System.out.println("O CEP informado é válido.");
		}
	}
}
