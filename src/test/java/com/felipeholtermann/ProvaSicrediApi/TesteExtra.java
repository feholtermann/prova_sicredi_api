package com.felipeholtermann.ProvaSicrediApi;

import static io.restassured.RestAssured.given;

import java.util.Scanner;

import org.junit.Test;

import io.restassured.response.Response;

public class TesteExtra {
	public Scanner entrada;
	@Test
	public void validacaoEndereco() {
		System.out.println("Teste de busca pelo endereço");
		entrada = new Scanner(System.in);
		System.out.println("Digite o Estado");
		String estado = entrada.next();

		System.out.println("Digite a Cidade");
		String cidade = entrada.next();

		System.out.println("Digite o Logradouro");
		String logradouro = entrada.next();

		Response response = given().relaxedHTTPSValidation().when()
				.get("https://viacep.com.br/ws/" + estado + "/" + cidade + "/" + logradouro + "/json/");
		response.getStatusCode();

		if (response.getStatusCode() == 400) {
			System.out.println("Não foi possível obter nenhum resultado com os dados informados.");
		} else if (!response.body().asString().contains("erro") && (response.getStatusCode() == 200)) {
			System.out.println("Foi possível obter os seguintes resultados com os dados informados: "
					+ response.body().asString());
		}
	}
}
